# README

The purpose of this application is to provide information on scotch whisky distilleries and reviews about whiskies they produce.

This README informs on how to install this application locally. An online version of the application can be found at https://www.whiksyinfo2.herokuapp.com

Follow the following steps (it is expected that a UNIX-based OS is used and at least Ruby 2.4.1 and Ruby on Rails 5.2.1 & PostgreSQL are installed):

Download and extract the .zip file and navigate to the root folder in Terminal.

run "bundle install" to install all gems and dependencies from the Gemfile (run "gem install bundler" if not installed)

Open /config/database.yml and change username & password to your local data- base details (for test, development & production)

run "rake db:migrate" to create the tables

run "rake db:seed" to seed the tables with data from the CSV files

run "rails s" to start the local server

Open a browser window and navigate to "localhost:3000" to view & start using the application. Even though this runs the application locally, make sure you are connected to the internet so that the application can receive data e.g. from the Bootstrap CDN or from Google Maps API.

For creating the admin account, please email me to get instructions.

Have fun :) For any questions, email j.fischer.17@aberdeen.ac.uk

Sources for Authlogic functionalities use code modified and adapted from:
https://github.com/thevole/AuthLogic-Email-Verification-Example
https://github.com/binarylogic/authlogic_example
http://www.nathancolgate.com/post/184694426/adding-email-and-user-verification-to-authlogic
https://www.sitepoint.com/rails-authentication-with-authlogic/

The shopping cart functionalities use code adapted and modified from:
https://richonrails.com/articles/building-a-shopping-cart-in-ruby-on-rails
