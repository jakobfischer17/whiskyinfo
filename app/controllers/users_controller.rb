class UsersController < ApplicationController

  def index
    require_admin
    @user = User.all
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(users_params)
    if @user.save
      @user.deliver_verification_instructions!
      redirect_to(root_path, :notice => 'You must now confirm your account. Check your email.')
    else
      render :new
    end
  end

  def destroy
    require_admin
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_index_url, notice: 'User was successfully deleted.' }
      format.json { head :no_content }
    end
  end
  private

  def users_params
    params.require(:user).permit(:email, :password, :password_confirmation)
  end
end
