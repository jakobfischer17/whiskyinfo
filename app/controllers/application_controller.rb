class ApplicationController < ActionController::Base

  def current_order
    if !session[:order_id].nil?
      Order.find(session[:order_id])
    else
      Order.new
    end
  end

  private
  def current_user_session
    return @current_user_session if defined?(@current_user_session)
    @current_user_session = UserSession.find
  end

  def current_user
    return @current_user if defined?(@current_user)
    @current_user = current_user_session && current_user_session.user
  end

  #restricts access only to logged in users
  def require_user
    unless current_user
      flash[:notice] = "You must be logged in as a verified user to access this page"
      redirect_to new_user_url
      return false
    end
  end

  def require_no_user
    if current_user
      flash[:notice] = "You must be logged out to access this page"
      redirect_to account_url
      return false
    end
  end

#user needs to be admin to view admin dashboard
  def require_admin
    email = User.where(email: current_user.email).pluck.flatten[1]
    unless  email == "admin@admin.admin"
      flash[:error] = "You must be logged in as an admin to access this page"
      redirect_to root_path
      return false
    end
  end

  helper_method :current_user_session, :current_user, :require_user, :require_admin, :require_no_user, :current_order
end
