class UserVerificationsController < ApplicationController
  before_action :load_user_using_perishable_token

  def show
    if @user
      @user.verify!
      flash[:notice] = "Thanks for verifying your account. You can now sign in with your login details."
    end
    redirect_to(root_path)
  end

  private

  def load_user_using_perishable_token
    @user = User.find_using_perishable_token(params[:id])
    flash[:notice] = 'Unable to find your account' unless @user
  end
end
