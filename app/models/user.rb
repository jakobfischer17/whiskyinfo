class User < ApplicationRecord
  acts_as_authentic do |c|
    c.crypto_provider = Authlogic::CryptoProviders::Sha512
  end

  def deliver_password_reset_instructions!
    reset_perishable_token!
    PasswordResetMailer.reset_email(self).deliver_now
  end

  def deliver_verification_instructions!
    reset_perishable_token!
    PasswordResetMailer.account_verification_instructions(self).deliver_now
  end

  def verify!
    self.verified = true
    self.save
  end
end
