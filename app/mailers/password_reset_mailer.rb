class PasswordResetMailer < ApplicationMailer
  def reset_email(user)
    @user = user
    mail(to: @user.email, subject: 'Password reset instructions')
  end

  def account_verification_instructions(user)
    @confirm_account_url = user_verification_url(user.perishable_token)
    mail :to => user.email, :subject => "Validate your details"
  end
end
