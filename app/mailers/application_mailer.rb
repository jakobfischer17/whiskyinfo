class ApplicationMailer < ActionMailer::Base
  default from: 'awadass3@outlook.com'
  layout 'mailer'
end
