json.extract! review, :id, :distillery, :name, :category, :reviewpoints, :price, :description, :created_at, :updated_at
json.url review_url(review, format: :json)
