json.extract! distillery, :id, :distillery, :body, :sweetness, :smoky, :medicinal, :tobacco, :honey, : , :created_at, :updated_at
json.url distillery_url(distillery, format: :json)
