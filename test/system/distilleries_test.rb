require "application_system_test_case"

class DistilleriesTest < ApplicationSystemTestCase
  setup do
    @distillery = distilleries(:one)
  end

  test "visiting the index" do
    visit distilleries_url
    assert_selector "h1", text: "Distilleries"
  end

  test "creating a Distillery" do
    visit distilleries_url
    click_on "New Distillery"

    fill_in " ", with: @distillery. 
    fill_in "Body", with: @distillery.body
    fill_in "Distillery", with: @distillery.distillery
    fill_in "Honey", with: @distillery.honey
    fill_in "Medicinal", with: @distillery.medicinal
    fill_in "Smoky", with: @distillery.smoky
    fill_in "Sweetness", with: @distillery.sweetness
    fill_in "Tobacco", with: @distillery.tobacco
    click_on "Create Distillery"

    assert_text "Distillery was successfully created"
    click_on "Back"
  end

  test "updating a Distillery" do
    visit distilleries_url
    click_on "Edit", match: :first

    fill_in " ", with: @distillery. 
    fill_in "Body", with: @distillery.body
    fill_in "Distillery", with: @distillery.distillery
    fill_in "Honey", with: @distillery.honey
    fill_in "Medicinal", with: @distillery.medicinal
    fill_in "Smoky", with: @distillery.smoky
    fill_in "Sweetness", with: @distillery.sweetness
    fill_in "Tobacco", with: @distillery.tobacco
    click_on "Update Distillery"

    assert_text "Distillery was successfully updated"
    click_on "Back"
  end

  test "destroying a Distillery" do
    visit distilleries_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Distillery was successfully destroyed"
  end
end
