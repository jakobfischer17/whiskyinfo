class CreateDistilleries < ActiveRecord::Migration[5.2]
  def change
    create_table :distilleries do |t|
      t.string :distillery
      t.integer :body
      t.integer :sweetness
      t.integer :smoky
      t.integer :medicinal
      t.integer :tobacco
      t.integer :honey
      t.string :spicy
      t.integer :winey
      t.integer :nutty
      t.integer :malty
      t.integer :fruity
      t.integer :floral
      t.string :postcode
      t.float :latitude
      t.float :longtitude
      
      t.timestamps
    end
  end
end
