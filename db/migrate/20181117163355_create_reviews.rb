class CreateReviews < ActiveRecord::Migration[5.2]
  def change
    create_table :reviews do |t|
      t.string :distillery
      t.text :name
      t.string :category
      t.integer :reviewpoints
      t.float :price
      t.text :description

      t.timestamps
    end
  end
end
