class AddForeignkeytoReviews < ActiveRecord::Migration[5.2]
  def change
    remove_column :distilleries, :distillery_id
    add_column :reviews, :distillery_id, :integer
    add_index :reviews, :distillery_id
  end
end
