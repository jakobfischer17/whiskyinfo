class AddDistilleryIdToIstilleries < ActiveRecord::Migration[5.2]
  def change
    add_column :distilleries, :distillery_id, :integer
  end
end
