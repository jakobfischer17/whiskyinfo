class AddIndextoDistilleries < ActiveRecord::Migration[5.2]
  def change
    add_column :distilleries, :distillery_id, :integer
    add_index :distilleries, :distillery_id
  end
end
