# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
require 'csv'

Distillery.destroy_all
Review.destroy_all
Product.destroy_all
OrderStatus.destroy_all

Product.create! id: 1, name: "Glenfiddich 12 Years Old", price: 29.99, active: true
Product.create! id: 2, name: "The Balvenie Double Wood 12 Year Old", price: 44.95, active: true
Product.create! id: 3, name: "Aberlour 10 Year Old Double Cask", price: 20.00, active: true
Product.create! id: 4, name: "Glenmorangie 10 Year Old", price: 40.00, active: true
Product.create! id: 5, name: "Talisker 10 year old", price: 34.95, active: true
Product.create! id: 6, name: "Carton of Strawberries", price: 1.99, active: true
Product.create! id: 7, name: "Dalmore 15 Year Old", price: 42.49, active: true
Product.create! id: 8, name: "Glenfarclas 25 Year Old", price: 114.73, active: true
Product.create! id: 9, name: "Highland Park 25 Year Old", price: 321.45, active: true
Product.create! id: 10, name: "Auchentoshan 21 Year Old", price: 120.00, active: true
Product.create! id: 11, name: "Glenfiddich 21 Year Old", price: 114.95, active: true
Product.create! id: 12, name: "Highland Park 21 Year Old", price: 342.00, active: true

OrderStatus.create! id: 1, name: "In Progress"
OrderStatus.create! id: 2, name: "Placed"
OrderStatus.create! id: 3, name: "Shipped"
OrderStatus.create! id: 4, name: "Cancelled"

csv_text = File.read(Rails.root.join('lib', 'assets', 'distillerycharacter1.csv'))
csv = CSV.parse(csv_text, :headers => true, :encoding => 'ISO-8859-1')
csv.each do |row|
  c = Distillery.new
  c.distillery = row['Distillery']
  c.body = row['Body']
  c.sweetness = row['Sweetness']
  c.smoky = row['Smoky']
  c.medicinal = row['Medicinal']
  c.tobacco = row['Tobacco']
  c.honey = row['Honey']
  c.spicy = row['Spicy']
  c.winey = row['Winey']
  c.nutty = row['Nutty']
  c.malty = row['Malty']
  c.fruity = row['Fruity']
  c.floral = row['Floral']
  c.postcode = row['Postcode']
  c.latitude = row['Latitude']
  c.longtitude = row['Longtitude']

  puts "#{c.distillery} saved"
  c.save
end

csv_text = File.read(Rails.root.join('lib', 'assets', 'whiskyreview1.csv'))
csv = CSV.parse(csv_text, :headers => true, :encoding => 'ISO-8859-1')
csv.each do |row|

  distillery = Distillery.find_by(distillery: row['Distillery'])
  if distillery != nil
    d = Review.new
    d.distillery_id = distillery.id
    d.distillery = row['Distillery']
    d.name = row['Name']
    d.category = row['Category']
    d.reviewpoints = row['ReviewPoints']
    d.price = row['Price']
    d.description = row['Description']
    d.save
    puts "#{d.name} saved"
  end
end
