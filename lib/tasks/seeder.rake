require 'csv'
namespace :seeder do
  task seed: :environment do
    CSV.foreach('lib/assets/whiskyreview1.csv', :headers => true) do |item|
      Review.create!(
        distillery: 'test',
        name: 'test',
        category: 'test',
        reviewpoints: 20,
        price: 22.2,
        description: 'test'
      )
    end
  end
end
