Rails.application.routes.draw do
#reviews and distilleries
  resources :reviews
  resources :distilleries
  root to: 'distilleries#index'
#users
  resources :users, only: [ :new, :create, :destroy]
  get '/users/index'
  get '/pages/index'
  resources :user_sessions, only: [:create, :destroy]
  resources :password_resets, only: [:new, :create, :edit, :update]
  resources :user_verifications, :only => [:show]
  delete '/sign_out', to: 'user_sessions#destroy', as: :sign_out
  get '/sign_in', to: 'user_sessions#new', as: :sign_in
#shopping cart
  resources :products, only: [:index]
  resource :cart, only: [:show]
  resources :order_items, only: [:create, :update, :destroy], defaults: { format: 'js' }
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
